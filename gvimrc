" Set the font used in GVim
let s:DefaultFontName = "Hack"
let s:DefaultFontSize = 11

let s:FontName = s:DefaultFontName
let s:FontSize = s:DefaultFontSize

function SetGuiFont (fontName, fontSize)
  if has("nvim")
    call GuiFont(a:fontName . ":h" . a:fontSize)
  else
    if has("win16") || has("win32")   " Windows
      let &guifont = a:fontName . ":h" . a:fontSize
    else                              " Linux
      let &guifont = a:fontName . " "  . a:fontSize
    endif
  endif
endfunction
command! UpdateFont call SetGuiFont(s:FontName, s:FontSize)

UpdateFont

" Functions to increase or decrease font size
function IncreaseGuiFont ()
  let s:FontSize += 1
  UpdateFont
endfunction
command! IncreaseFontSize call IncreaseGuiFont()
map <silent> <M-p> :IncreaseFontSize<CR>

function DecreaseGuiFont ()
  let s:FontSize -= 1
  UpdateFont
endfunction
command! DecreaseFontSize call DecreaseGuiFont()
map <silent> <M-i> :DecreaseFontSize<CR>

function ResetGuiFont ()
  let s:FontName = s:DefaultFontName
  let s:FontSize = s:DefaultFontSize
  UpdateFont
endfunction
command! ResetFont call ResetGuiFont()
map <silent> <M-o> :ResetFont<CR>

" Set a reasonable number of lines and columns
set lines=25
set columns=100

" Disable all GUI Options
set guioptions=

" Disable cursor blinking
set guicursor+=a:blinkon0

" Set a light background
set background=light

" Maximize window
function MaximizeWindow()
  let &lines   = 999
  let &columns = 999
endfunction
command! MaximizeWindow call MaximizeWindow()
map <silent> <M-m> :MaximizeWindow<CR>

" Minimize window
function MinimizeWindow()
  let &lines   = 25
  let &columns = 100
endfunction
command! MinimizeWindow call MinimizeWindow()
map <silent> <M-n> :MinimizeWindow<CR>
