if (!exists('g:is_neovim'))
  let g:is_neovim = 0
end

set nocompatible          " Disable compatibility with Vi
syntax on                 " Enable syntax highlighting
filetype plugin indent on " Enable automatic indentation
scriptencoding utf-8

" Store .swp, undo, backup files inside personal directory
if has('unix')
  set undodir=~/.vim/undo//
elseif has('win32')
  set undodir=~/vimfiles/undo//
endif

" FILE Setup
set autoread              " Automatically update externally changed files
set nowritebackup         " Don't backup current file before saving
set noswapfile            " Don't create a swap file when opening
set encoding=utf-8        " Encode vim text in utf-8
set fileformat=unix       " Set UNIX file endings
set fileencoding=utf-8    " Encode file text in utf-8
set fileencodings=utf8,ucs-bom,prc
set undofile              " Enable undofile
set undolevels=1000       " Set the number of undos to save
set undoreload=10000      " Set the number of lines for undo to save
set wildignore=*.svn,*CVS,*.git
set wildignore+=*.o,*.a,*.class,*.mo,*.la,*.so,*.obj,*.swp
set wildignore+=*.jpg,*.png,*.xpm,*.gif,*.pdf,*.bak,*.beam,*.exe,*.mp3,*.dll
set wildignore+=*.aux,*.fdb_latexmk,*.fls,*.out,*.toc,*.synctex,*.synctex.gz
set suffixes+=*.old,*.log " Low priority files

" VISUAL Settings
set number                                               " Set line numbers
set laststatus=2                                         " Always show statusline
set statusline=\ %f\ %<%h%m%r%=%l\:%c\ 
set wildmenu
set list listchars=tab:¬\ ,extends:»,precedes:«,trail:·
if (has("termguicolors"))
  if (g:is_neovim)
    set termguicolors
  end
endif

" TEXT Settings
set nowrap                     " Disable soft wrapping
set wrapmargin=0               " Wrap text at 0 characters from right border
set textwidth=0                " Don't hard wrap text by default
set backspace=indent,eol,start " Set a proper behavior for backspace

noremap! <C-BS> <C-w>

" PANE Settings
set splitbelow " Split panes down
set splitright " Split panes right

" MOUSE Settings
set mouse=a " Enable support for mouse
set mousehide " Hide mouse when typing
if (!g:is_neovim)
  set ttymouse=sgr
end

" Scroll half screen using CTRL+Mouse Wheel
noremap  <C-ScrollWheelUp>   <C-U>
noremap  <C-ScrollWheelDown> <C-D>
inoremap <C-ScrollWheelUp>   <C-O><C-U>
inoremap <C-ScrollWheelDown> <C-O><C-D>

" Scroll full screen using SHIFT+Mouse Wheel
noremap  <S-ScrollWheelUp>   <C-B>
noremap  <S-ScrollWheelDown> <C-F>
inoremap <S-ScrollWheelUp>   <C-O><C-B>
inoremap <S-ScrollWheelDown> <C-O><C-F>

" TAB Setup
set tabstop=4    " Show existing tab with 4 spaces width
set shiftwidth=4 " When indenting with '>', use 4 spaces width
set expandtab    " On pressing tab, insert spaces
set smarttab     " Enable smart tab

" FILE Autocommands
autocmd BufNewFile,BufRead *.vho          setlocal filetype=vhdl
autocmd BufNewFile,BufRead *.veo          setlocal filetype=verilog_systemverilog
autocmd BufNewFile,BufRead *.param_config setlocal filetype=json
autocmd FileType gitcommit setlocal nofoldenable
autocmd FileType yaml setlocal tabstop=4 shiftwidth=4

autocmd BufReadPost,FileReadPost *
    \   if &readonly
    \|      setlocal nolist
    \|      setlocal nomodifiable
    \|  else
    \|      setlocal list
    \|      setlocal modifiable
    \|  endif

autocmd VimResized * :wincmd =

" SEARCH Setup
set hlsearch   " Highlight search results
set incsearch  " Enable incremental search
set ignorecase " Search case insensitive
set smartcase  " Search case sensitive if there is an uppercase character
map <silent> <C-H> :set hlsearch!<CR>
nnoremap g/ /\<\><Left><Left>
vnoremap g/ /\<\><Left><Left>

" SCROLLING Setup
set scrolloff=5          " Always show some lines at top and bottom
nnoremap <silent> <s-l> 5zl
vnoremap <silent> <s-l> 5zl
nnoremap <silent> <s-h> 5zh
vnoremap <silent> <s-h> 5zh

" FOLDING Setup
set foldmethod=syntax    " Automatically fold using syntax
nnoremap <silent> <space> za

" TAB Setup
nnoremap <silent> gb gT
nnoremap <silent> <c-w><PageUp>   :tabmove -<CR>
nnoremap <silent> <c-w><PageDown> :tabmove +<CR>

" CLIPBOARD Setup
set clipboard=unnamed

""" FUNCTIONS ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function! TrimWhiteSpace()
  %s/\s\+$//e
endfunction
command! TrimWhiteSpace call TrimWhiteSpace()

""" PLUGINS ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

" PLUGIN ultisnips
let g:UltiSnipsExpandTrigger       = "<C-j>"
let g:UltiSnipsJumpForwardTrigger  = "<C-j>"
let g:UltiSnipsJumpBackwardTrigger = "<C-k>"
let g:UltiSnipsEditSplit           = "vertical"

" PLUGIN snipmate
imap <C-j> <Plug>snipMateNextOrTrigger
smap <C-j> <Plug>snipMateNextOrTrigger

" PLUGIN vim-session
let g:session_lock_enabled         = 1
let g:session_extension            = '.vimsession'
let g:session_autoload             = 'no'
let g:session_autosave             = 'yes'
let g:session_autosave_periodic    = 0
let g:session_autosave_silent      = 1
let g:session_verbose_messages     = 0
let g:session_persist_colors       = 0
let g:session_persist_globals      = ['&textwidth']

" PLUGIN vim-commentary
autocmd FileType vhdl setlocal commentstring=--%s
autocmd FileType systemverilog setlocal commentstring=//%s
autocmd FileType verilog_systemverilog setlocal commentstring=//%s

" PLUGIN vim-easy-align
nmap ga <Plug>(EasyAlign)
xmap ga <Plug>(EasyAlign)

" PLUGIN vim-maximizer
let g:maximizer_set_default_mapping = 0
nnoremap <c-w>z :MaximizerToggle<CR>
vnoremap <c-w>z :MaximizerToggle<CR>gv

" PLUGIN vim-solarized8
let g:solarized_visibility       = "high"
let g:solarized_diffmode         = "normal"
let g:solarized_termtrans        = 1
let g:solarized_statusline       = "flat"
let g:solarized_term_italics     = 1
let g:solarized_old_cursor_style = 1
let g:solarized_extra_hi_groups  = 1
if (g:is_neovim)
  let g:solarized_use16          = 0
else
  let g:solarized_use16          = 1
endif

" PLUGINS List
call plug#begin()
Plug 'tomtom/tlib_vim'                  |
    Plug 'MarcWeber/vim-addon-mw-utils' |
    Plug 'garbas/vim-snipmate'          |
    Plug 'emacann/vim-snippets'
Plug 'lifepillar/vim-solarized8'
Plug 'danro/rename.vim'
Plug 'junegunn/vim-easy-align'
Plug 'skywind3000/asyncrun.vim'
Plug 'szw/vim-maximizer'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'vhda/verilog_systemverilog.vim'
Plug 'xolox/vim-misc' |
    Plug 'xolox/vim-session'
if executable('rg')
  Plug 'jremmen/vim-ripgrep'
endif
call plug#end()

" PLUGIN verilog_systemverilog
let g:verilog_indent_assign_fix    = 0
let g:verilog_disable_indent_lst   = "eos,preproc"
let g:verilog_syntax_fold_lst      = "instance,block_named,function"
let g:verilog_efm_level            = "lint"
let g:verilog_efm_uvm_lst          = "all"
let g:verilog_efm_quickfix_clean   = 1

" Set the console colorscheme
set background=dark
colorscheme solarized8_flat

" Easier diff
function! DiffToggle()
  if &diff
    set noscrollbind
    windo diffoff
  else
    set scrollbind
    windo diffthis
  endif
endfunction
command! DiffToggle call DiffToggle()

" Easy Configuration Reload
command! ReloadCfg source $MYVIMRC | echom "Configuration reloaded!"

" P4 Edit/Revert - Must be synchronous
command! Edit   silent !p4 edit %
command! Revert silent !p4 revert %

" P4 Add - Can be asynchronous
command! Add AsyncRun p4 add %
