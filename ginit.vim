" Neovim configuration file
if has("win32")
  source $APPDATA/../Local/nvim/gvimrc
else
  source ~/.config/nvim/gvimrc
endif
