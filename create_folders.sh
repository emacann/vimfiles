#!/bin/sh
SCRIPTDIR=`dirname "$(readlink -f "$0")"`

mkdir -p $SCRIPTDIR/swap
mkdir -p $SCRIPTDIR/backup
mkdir -p $SCRIPTDIR/undo
