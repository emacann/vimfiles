" Neovim configuration file

let $NVIM_TUI_ENABLE_TRUE_COLOR = 1
let g:is_neovim = 1

if has("win32")
  source $APPDATA/../Local/nvim/vimrc
else
  source ~/.config/nvim/vimrc
endif
